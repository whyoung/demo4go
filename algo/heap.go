package algo

//struct heap
type Heap struct {
	data []int
}

//add param val
func (h *Heap) Add(val int) {
	h.data = append(h.data, val)
	size := len(h.data)
	if size == 1 {
		return
	}
	h.siftUp(size - 1)
}

//remove heap top
func (h *Heap) Remove() {
	size := len(h.data)
	if size == 0 {
		return
	}
	if size == 1 {
		h.data = []int{}
		return
	}
	h.data[0] = h.data[size - 1]
	h.siftDown(0)
}

//add param val
func heapify(data []int) *Heap {
	heap := &Heap{
		data: data,
	}
	size := len(data)
	if size > 1 {
		for idx := parentIdxOf(size - 1); idx >=0; idx -- {
			heap.siftDown(idx)
		}
	}
	return heap
}

func (h *Heap) siftUp(index int) {
	for idx := index; idx > 0;  {
		parentIdx := parentIdxOf(index)
		if h.data[parentIdx] < h.data[index] {
			h.data[parentIdx], h.data[index] = h.data[index], h.data[parentIdx]
			idx = parentIdx
		}
	}
}

func (h *Heap) siftDown(index int) {
	size := len(h.data)
	for idx := index; idx < size; {
		leftIndex := idx << 1 + 1
		rightIdx := leftIndex + 1
		if leftIndex <= size - 1 && h.data[leftIndex] > h.data[idx] {
			h.data[leftIndex], h.data[idx] = h.data[idx], h.data[leftIndex]
			idx = leftIndex
		} else if rightIdx <= size - 1 && h.data[rightIdx] > h.data[idx] {
			h.data[rightIdx], h.data[idx] = h.data[idx], h.data[rightIdx]
			idx = rightIdx
		}
	}
}

func parentIdxOf(currentIdx int) int {
	if currentIdx <= 0 {
		return -1
	}
	if currentIdx&1 == 1 {
		return (currentIdx - 1) >> 1
	}
	return currentIdx>>1 - 1
}