package algo

//匹配模式
type mode struct {
	b byte
	cm bool
}

func isMatch(s, p string) bool {
	modes := parse(p)
	l1, l2 := len(s), len(*modes)
	return adapts(&s, 0, l1,  modes, 0, l2)
}

//模式解析，匹配字符和匹配个数
func parse(p string) *[]*mode {
	l := len(p)
	if l == 0 {
		return nil
	}
	var ret []*mode
	for i := 0; i<l; {
		if p[i] >= 'a' && p[i] <= 'z' || p[i] == '.' {
			m := &mode{
				b: p[i],
				cm: false,
			}
			i++
			if i < l && p[i] == '*' {
				m.cm = true
				i++
			}
			ret = append(ret, m)
		}
	}
	return &ret
}

func adapts(s *string, idx1, l1 int, modes *[]*mode, idx2, l2 int) bool {
	/*
	结束条件
	1.模式已经结束
	2.待匹配字符已经结束
	 */
	if idx1 >= l1 && idx2 >= l2 {
		return true
	}
	if idx1 >= l1 {
		for ;idx2 < l2 && !(*modes)[idx2].cm; idx2++ {
			//如果模式未结束，判断剩余模式是否可以匹配""
			return false
		}
		return true
	} else if idx2 >= l2 {
		return false
	}

	for ; idx1<l1; {
		m := (*modes)[idx2]
		if m.b == '.' {
			if m.cm {
				for ; idx1 <= l1;idx1++  {
					//从idx1位开始匹配，直到最后一位
					if adapts(s, idx1, l1, modes, idx2+1, l2) {
						return true
					}
				}
			} else {
				//匹配了当前位，从idx1的下一位开始匹配
				return adapts(s, idx1 +1, l1, modes, idx2 + 1, l2)
			}
		} else {
			if m.cm {
				if (*s)[idx1] != m.b {
					//当前位不匹配，直接跳过，匹配下一个
					return adapts(s, idx1, l1, modes, idx2+1, l2)
				}
				//当前匹配，但优先匹配""，
				if adapts(s, idx1,l1, modes, idx2 + 1, l2) {
					return true
				}
				//依次匹配符合的字符
				for ;idx1 < l1 && (*s)[idx1] == m.b;idx1++ {
					if adapts(s, idx1 + 1,l1, modes, idx2 + 1, l2) {
						return true
					}
				}
				return false
			} else {
				//匹配下一个字符和模式
				return (*s)[idx1] == m.b && adapts(s, idx1+1, l1, modes, idx2+1, l2)
			}
		}
	}
	return false
}