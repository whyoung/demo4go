package algo

import (
	"fmt"
	"testing"
)

var (
	proposers []*proposer
	acceptors []*acceptor
)

//func init() {
//	proposers = make([]*proposer, 3)
//	proposers[0] = &proposer{id:"C1"}
//	proposers[1] = &proposer{id:"C2"}
//	proposers[2] = &proposer{id:"C3"}
//
//	acceptors = make([]*acceptor, 3)
//	acceptors[0] = &acceptor{id:"G1", recv:make([]proposal, 8)}
//	acceptors[1] = &acceptor{id:"G2", recv:make([]proposal, 8)}
//	acceptors[2] = &acceptor{id:"G3", recv:make([]proposal, 8)}
//}
//
//proposal
type proposal struct {
	id int64
	//key string
	value string
}

//
//proposer
type proposer struct {
	id string
}

//acceptor
type acceptor struct {
	id     string
	stored int64
	value  string
}

func (p *proposer) propose(pp proposal) {
	cnt, null, resped := 0, false, make([]*acceptor, 8)
	//max_value
	maxId, maxValue := int64(0), ""
	for _, a := range acceptors {
		r, ok := a.accept(pp)
		if ok {
			cnt++
			if r.id != 0 {
				if r.id > maxId {
					maxValue = r.value
				}
				null = false
			}
			resped = append(resped, a)
		}
	}
	l := len(acceptors)
	if cnt < l/2 {
		//less than half
		return
	}
	if !null {
		pp.value = maxValue
	}
	p.confirm(resped, pp)
}

func (p *proposer) confirm(resped []*acceptor, pp proposal) {
	for _, a := range resped {
		a.accept(pp)
	}
}

func (a *acceptor) accept(pp proposal) (proposal, bool) {
	oldId := a.stored
	if a.stored <= pp.id {
		a.value = pp.value
		a.stored = pp.id
		return proposal{oldId, a.value}, true
	}
	return proposal{}, false
}

//
//type response struct {
//	accepted bool
//	id int64
//	value string
//}
//
//func (p *proposer) propose(a *acceptor, pp proposal) response {
//	if a.accepted {
//		resp := response{
//			accepted: false,
//			id:       a.accept.id,
//			value:    a.accept.value,
//		}
//		if pp.id > a.accept.id {
//			a.accepted = false
//			resp.accepted = true
//		}
//		return resp
//	}
//	l := len(a.recv)
//	if l > 0 {
//		last := a.recv[l - 1]
//		if last.id > pp.id {
//			return response{
//				accepted: false,
//			}
//		}
//	}
//	a.recv = append(a.recv, pp)
//	return response{
//		accepted: true,
//	}
//}
//
//func (p *proposer) confirm(a *acceptor, pp proposal) response {
//	last := a.recv[len(a.recv) - 1]
//	if last.id > pp.id {
//		return response{
//			accepted: false,
//			id:       last.id,
//		}
//	}
//	a.accepted = true
//	a.accept = last
//	return response{
//		accepted: true,
//		id: last.id,
//		value: last.value,
//	}
//}
//
//func NewId() int64 {
//	return time.Now().Unix()
//}
//
//func TestTest(t *testing.T) {
//	c1, c2, _ := proposers[0], proposers[1], proposers[2]
//	g1, g2, g3 := acceptors[0], acceptors[1], acceptors[2]
//
//	p1 := proposal{
//		id:    1,
//		value: "20191022",
//	}
//	fmt.Println(c1.propose(g1, p1))
//	fmt.Println(c1.propose(g2, p1))
//
//	p2 := proposal{
//		id:    2,
//		value: "20191023",
//	}
//
//	fmt.Println(c2.propose(g2, p2))
//	fmt.Println(c2.propose(g3, p2))
//
//	fmt.Println(c1.confirm(g1, p1))
//	fmt.Println(c1.confirm(g2, p1))
//
//	fmt.Println(c2.confirm(g2, p2))
//	fmt.Println(c2.confirm(g3, p2))
//
//	p3 := proposal{
//		id:    3,
//		value: "20191024",
//	}
//
//	r311 := c1.propose(g1, p3)
//	r312 := c1.propose(g1, p3)
//	if !r311.accepted && !r312.accepted {
//		v := r311.value
//		if r311.id < r312.id {
//			v = r312.value
//		}
//		p3.value = v
//
//		fmt.Println(c1.propose(g1, p3))
//		fmt.Println(c1.propose(g2, p3))
//
//		fmt.Println(c1.confirm(g1, p3))
//		fmt.Println(c1.confirm(g2, p3))
//	}
//}
//var nodes []*node
//
//func init() {
//	nodes = make([]*node, 3)
//	for i := 1; i<=3; i++ {
//		nodes[i-1] = NewNode(fmt.Sprintf("%d", i))
//	}
//	rand.Seed(time.Now().UnixNano())
//}
//
//
//type proposal struct {
//	id int64
//	key string
//	value string
//}
//
//type proposer struct {
//}
//
//func NewId() int64 {
//	return time.Now().Unix()
//}
//
//
//func (p *proposer) propose(key, value, ignore string) {
//	np := proposal{
//		id:NewId(),
//		key:key,
//		value:value,
//	}
//	for _, a := range nodes {
//		if a.id == ignore {
//			continue
//		}
//		fmt.Println(a.accept(np))
//	}
//}
//
//func (a *acceptor) accept(p proposal) proposal {
//	ps, ok := a.cache[p.key]
//	if ok {
//
//	} else {
//		a.cache[p.key] = proposal{
//			id:p.id,
//			key:p.key,
//			value:p.value,
//		}
//	}
//	return ps
//}
//
//type acceptor struct {
//	cache map[string]proposal
//}
//
//type node struct {
//	*proposer
//	*acceptor
//	id string
//}
//
//func NewNode(id string) *node {
//	return &node{
//		id: id,
//		acceptor: &acceptor{
//			cache:  make(map[string]proposal),
//		},
//	}
//}
//
//func RandNode() *node {
//	return nodes[rand.Intn(3)]
//}
//
//func TestPaxos(t *testing.T) {
//	RandNode().propose("v", "1", "1")
//	RandNode().propose("v", "2", "3")
//	<-time.After(time.Minute)
//}

type cache interface {
	Get(k string) interface{}
	Set(k string, v interface{})
	Del(k string)
	Size() uint
}

type node struct {
	k    string
	v    interface{}
	next *node
	pre  *node
}

type LRU struct {
	capacity int
	cache    map[string]*node
	head     *node
	tail     *node
}

func NewLRUCache(capacity int) *LRU {
	c := &LRU{capacity: capacity}
	c.cache = make(map[string]*node, capacity)
	return c
}

func (c *LRU) Get(k string) interface{} {
	if n, ok := c.cache[k]; ok {
		if len(c.cache) > 1 && n != c.tail {
			if n == c.head {
				n.next.pre = nil
				c.head = n.next
			}
			n.pre = c.tail
			c.tail.next = n
			c.tail = n
			n.next = nil
		}
		return n.v
	}
	return nil
}

func (c *LRU) Set(k string, v interface{}) {
	if n, ok := c.cache[k]; ok {
		n.v = v
		if c.tail != n {
			if c.head == n {
				c.head = n.next
				c.head.pre = nil
			} else {
				n.pre.next = n.next
				n.next.pre = n.pre
			}
			c.tail.next = n
			n.pre = c.tail
			c.tail = n
			c.tail.next = nil
		}
	} else {
		n := &node{k: k, v: v, next: nil, pre: c.tail}
		c.cache[k] = n
		if c.tail == nil {
			c.head, c.tail = n, n
		} else {
			c.tail.next = n
			c.tail = n
			if len(c.cache) > c.capacity {
				delete(c.cache, c.head.k)
				c.head = c.head.next
				c.head.pre = nil
			}
		}
	}
}

func (c *LRU) Size() uint {
	return uint(len(c.cache))
}

func (c *LRU) Del(k string) {
	if n, ok := c.cache[k]; ok {
		if len(c.cache) == 1 {
			c.head, c.tail = nil, nil
		} else if n == c.head {
			c.head = c.head.next
			c.head.next.pre = nil
			c.head = c.head.next
		} else if n == c.tail {
			c.tail = c.tail.pre
			c.tail.pre.next = nil
			c.tail = c.tail.pre
		} else {
			n.pre.next = n.next
			n.next.pre = n.pre
		}
		delete(c.cache, k)
	}
}

func (c *LRU) print() {
	n := c.head
	for n != nil {
		fmt.Printf("(%s, %v)\t", n.k, n.v)
		n = n.next
	}
	fmt.Println()
}

func TestLRU(t *testing.T) {
	c := NewLRUCache(5)
	c.Set("1", 1)
	c.Set("2", 2)
	c.Set("3", 3)
	c.Set("4", 4)
	c.Set("5", 5)
	c.print()

	c.Set("6", 6)
	c.print()

	c.Set("3", 3)
	c.print()

	c.Set("2", "222")
	c.print()
	c.Get("2")
	c.print()

	c.Del("6")
	c.print()

	c.Get("4")
	c.print()

	c.Set("1", 1)
	c.print()

	c.Set("2", "2")
	c.print()
}
