package algo

import (
	"fmt"
	"testing"
)

func TestIsMatch(t *testing.T) {
	fmt.Println(isMatch("aa", "a*"))
	fmt.Println(isMatch("aab", "c*a*b"))
	fmt.Println(isMatch("aedweew", "ac*..*"))
	fmt.Println(isMatch("a", "ab*"))
	fmt.Println(isMatch("bbbba", ".*a*a"))
	fmt.Println(isMatch("ab", ".*.."))
}
