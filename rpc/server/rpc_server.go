package server

import (
	"bitbucket.org/whyoung/demo/rpc/registry"
	"github.com/micro/go-micro"
	s "github.com/micro/go-micro/server"
	"github.com/micro/go-micro/server/grpc"
)

func NewGRPCServer() micro.Option {
	return micro.Server(grpc.NewServer(
		//server 的设置优先级是高于micro的设置优先级
		s.Name("demo"),
		s.Address(":8000"),
		s.Registry(registry.NewZookeeperRegistry()),
		//s.Transport(tg.NewTransport()),
		//s.Codec("application/protobuf", cg.NewCodec),
	))
}
