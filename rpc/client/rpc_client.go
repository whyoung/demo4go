package client

import (
	"bitbucket.org/whyoung/demo/proto"
	"bitbucket.org/whyoung/demo/rpc/registry"
	"context"
	"fmt"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-micro/transport/grpc"
	"time"
)

type Client struct {
	options []micro.Option
}

func (c *Client) RegisterOption(option micro.Option) {
	c.options = append(c.options, option)
}

func (c *Client) NewRequest(opts ...client.CallOption) {
	srv := micro.NewService(c.options...)
	doGet(srv.Client(), opts...)
}

func doGet(cli client.Client, opts ...client.CallOption) {
	c := proto.NewPersonInfoService("demo", cli)
	resp, err := c.GetPerson(context.Background(), &proto.Pid{
		Id:                   23,
	}, opts...)

	if err == nil {
		fmt.Println(resp)
	} else {
		fmt.Println("---------------------------------------")
		panic(err)
	}
}

func NewGRPCClient() micro.Option {
	return micro.Client(client.NewClient(
		client.RequestTimeout(time.Minute * 5),
		client.Registry(registry.NewZookeeperRegistry()),
		client.Transport(grpc.NewTransport()),
		//client.Codec("application/protobuf", cg.NewCodec),
		//client.Selector(selector.NewSelector(
		//	selector.SetStrategy(selector.Random),
		//	)),
		))
}
