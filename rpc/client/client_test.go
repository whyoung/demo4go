package client

import (
	"bitbucket.org/whyoung/demo/rpc/registry"
	"bitbucket.org/whyoung/demo/rpc/transport"
	"testing"
)

func TestClient(t *testing.T) {
	c := &Client{}
	c.NewRequest()
}

/*
	registry: 	zookeeper
	transport:	http
 */
func TestClientWithZkRegistry(t *testing.T) {
	c := &Client{}
	c.RegisterOption(registry.NewZookeeperRegistryOption())
	c.NewRequest()
}

func TestClientWithQuicTransport(t *testing.T) {
	c := &Client{}
	c.RegisterOption(registry.NewZookeeperRegistryOption())
	c.RegisterOption(transport.QuicTransport())
	c.NewRequest()
}

func TestClientWithGRPCTransport(t *testing.T) {
	c := &Client{}
	c.RegisterOption(registry.NewZookeeperRegistryOption())
	c.RegisterOption(transport.GRPCTransport())
	c.NewRequest()
}

func TestMicroClient(t *testing.T) {
	c := &Client{}
	c.RegisterOption(NewGRPCClient())
	c.NewRequest()
}