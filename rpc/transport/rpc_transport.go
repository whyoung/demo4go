package transport

import (
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/transport/grpc"
	"github.com/micro/go-micro/transport/http"
	"github.com/micro/go-micro/transport/quic"
)

//default http
func HttpTransport() micro.Option {
	return micro.Transport(http.NewTransport())
}

//udp/http2
func QuicTransport() micro.Option {
	return micro.Transport(quic.NewTransport())
}

//tcp/http2
func GRPCTransport() micro.Option {
	return micro.Transport(grpc.NewTransport())
}