package registry

import (
	zookeeper "bitbucket.org/whyoung/demo/rpc/registry/zookeeper"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/registry"
	consul "github.com/micro/go-micro/registry/consul"
)

func NewConsulRegistryOption() micro.Option {
	return micro.Registry(consul.NewRegistry(
		registry.Addrs("127.0.0.1:8500"),
		))
}

func NewZookeeperRegistryOption() micro.Option {
	return micro.Registry(NewZookeeperRegistry())
}

func NewZookeeperRegistry() registry.Registry {
	return zookeeper.NewRegistry(
		registry.Addrs("127.0.0.1:2181"),
	)
}
