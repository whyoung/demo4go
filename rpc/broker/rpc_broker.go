package broker

import (
	"bitbucket.org/whyoung/demo/rpc/broker/redis"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/broker"
)

func NewRedisBroker() micro.Option {
	return micro.Broker(redis.NewBroker(
		broker.Addrs("vm:6379"),
		))
}
