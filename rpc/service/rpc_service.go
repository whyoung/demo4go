package service

import (
	pp "bitbucket.org/whyoung/demo/proto"
	"github.com/micro/go-micro"
)

type Service struct {
	options []micro.Option
}


func BaseDemoService() *Service {
	s := &Service{}
	s.RegisterOption(micro.Name("demo"))
	s.RegisterOption(micro.Address(":8888"))
	return s
}

func (s *Service)RegisterOption(option micro.Option) {
	s.options = append(s.options, option)
}

func (s *Service) StartService() {
	srv := micro.NewService(s.options...)

	//bind service impl with server
	if err := pp.RegisterPersonInfoHandler(srv.Server(), pp.NewDemoHandler()); err != nil {
		panic(err)
	}

	srv.Init()
	if err := srv.Run(); err != nil {
		panic(err)
	}
}
