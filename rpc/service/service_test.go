package service

import (
	"bitbucket.org/whyoung/demo/rpc/registry"
	"bitbucket.org/whyoung/demo/rpc/server"
	"bitbucket.org/whyoung/demo/rpc/transport"
	"testing"
)

/**
service的默认参数
Broker:    broker.DefaultBroker,  http broker
Cmd:       cmd.DefaultCmd,
Client:    client.DefaultClient,  rpc client
Service:    server.DefaultServer,  rpc server
Registry:  registry.DefaultRegistry,  mdns registry
Transport: transport.DefaultTransport, http transport
Context:   context.Background(),
 */
func TestDefaultService(t *testing.T) {
	BaseDemoService().StartService()
}

func TestServiceWithZkRegistry(t *testing.T) {
	s := BaseDemoService()

	/*
	zk 节点数据
	{
	  "name": "demo",
	  "version": "1570525613",
	  "metadata": null,
	  "endpoints": [
	    {
	      "name": "PersonInfo.GetPerson",
	      "request": {
	        "name": "Pid",
	        "type": "Pid",
	        "values": [
	          {
	            "name": "id",
	            "type": "int32",
	            "values": null
	          }
	        ]
	      },
	      "response": {
	        "name": "Person",
	        "type": "Person",
	        "values": [
	          {
	            "name": "age",
	            "type": "int32",
	            "values": null
	          },
	          {
	            "name": "name",
	            "type": "string",
	            "values": null
	          }
	        ]
	      },
	      "metadata": {
	        "stream": "false"
	      }
	    },
	    {
	      "name": "PersonInfo.GetPersonsByExample",
	      "request": {
	        "name": "Person",
	        "type": "Person",
	        "values": [
	          {
	            "name": "age",
	            "type": "int32",
	            "values": null
	          },
	          {
	            "name": "name",
	            "type": "string",
	            "values": null
	          }
	        ]
	      },
	      "response": {
	        "name": "Persons",
	        "type": "Persons",
	        "values": [
	          {
	            "name": "persons",
	            "type": "[]Person",
	            "values": null
	          }
	        ]
	      },
	      "metadata": {
	        "stream": "false"
	      }
	    }
	  ],
	  "nodes": [
	    {
	      "id": "demo-e191d8d3-b8c1-4071-a857-55ef77f356b8",
	      "address": "10.93.245.248:8888",
	      "metadata": {
	        "broker": "http",
	        "protocol": "mucp",
	        "registry": "zookeeper",
	        "server": "mucp",
	        "transport": "http"
	      }
	    }
	  ]
	}
	 */
	//node path: /micro-registry/demo/{id}
	//node info: get /micro-registry/demo/{id}
	s.RegisterOption(registry.NewZookeeperRegistryOption())
	s.StartService()
}


func TestServiceWithQuicTransport(t *testing.T) {
	s := BaseDemoService()
	s.RegisterOption(registry.NewZookeeperRegistryOption())
	s.RegisterOption(transport.QuicTransport())
}

func TestServiceWithGRPCTransport(t *testing.T) {
	s := BaseDemoService()
	s.RegisterOption(registry.NewZookeeperRegistryOption())
	s.RegisterOption(transport.GRPCTransport())
}

func TestServiceWithGRPCServer(t *testing.T) {
	s := BaseDemoService()
	s.RegisterOption(server.NewGRPCServer())
	//s.RegisterOption(registry.NewZookeeperRegistryOption())
}