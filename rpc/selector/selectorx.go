package selector

import (
	"github.com/micro/go-micro/client"
	"github.com/micro/go-micro/client/selector"
	"github.com/micro/go-micro/registry"
)

//选择指定服务器
func SpecServerFilter(ip string, port int) client.CallOption {
	return func(co *client.CallOptions) {
		co.SelectOptions = append(co.SelectOptions, func() func(*selector.SelectOptions) {
			return func(so *selector.SelectOptions) {
				so.Filters = append(so.Filters, func() selector.Filter {
					return func(old []*registry.Service) []*registry.Service {
						for _, service := range old {
							for _, node := range service.Nodes {
								if node.Address == ip && node.Port == port {
									//at most one node
									srv := new(registry.Service)
									srv.Nodes = []*registry.Node{node}
									return []*registry.Service{srv}
								}
							}
						}
						return nil
					}
				}())
			}
		}())
	}
}

