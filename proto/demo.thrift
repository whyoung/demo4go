namespace go proto

struct Person {
	1: required i32 age;
    2: required string name;
    //3: set<string> s; //go 没有set的实现，会使用 slice 代替，不会去重
}

exception Err {

}
service PersonInfo {
    Person GetPerson(1:i32 pid) throws (1:Err e)

    list<Person> GetPersonsByExample(1:Person p)

    void f1()

    void f2() throws (1:Err e)

    i32 f3(1:Person p, 2:i32 pid)
}