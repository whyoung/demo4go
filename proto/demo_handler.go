package proto

import (
	"context"
)

type demoHandler struct {
}

func (d demoHandler) GetPerson(ctx context.Context, p *Pid, pp *Person) error {
	pp.Age = 2
	pp.Name = "Donald Trump"
	//<-time.After(time.Minute)
	//panic("")
	return nil
}

func (d demoHandler) GetPersonsByExample(ctx context.Context, p *Person, pp *Persons) error {
	ppp := &Person{
		Age:                  2,
		Name:                 "Donald Trump",
	}
	pp.Persons = append(pp.Persons, ppp)
	return nil
}

func NewDemoHandler() PersonInfoHandler {
	return new(demoHandler)
}



