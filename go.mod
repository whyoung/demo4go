module bitbucket.org/whyoung/demo

go 1.13

require (
	github.com/apache/thrift v0.13.0
	github.com/golang/protobuf v1.3.2
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/gorilla/websocket v1.4.0
	github.com/julienschmidt/httprouter v1.2.0
	github.com/micro/go-micro v1.10.0
	github.com/mitchellh/hashstructure v1.0.0
	github.com/nats-io/nats-server/v2 v2.1.0 // indirect
	github.com/samuel/go-zookeeper v0.0.0-20190923202752-2cc03de413da
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80
)
