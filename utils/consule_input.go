package utils

import (
	"bufio"
	"os"
)

func read(ch chan string, quit string) {
	input := bufio.NewScanner(os.Stdin)
	for input.Scan() {
		line := input.Text()
		if line == quit {
			break
		}
		ch <- line
	}
}

