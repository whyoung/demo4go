package utils

import "time"
import "fmt"

const timeLayout = "2006-01-02 15:04:05"

//日期格式化
func ParseWithLocation(name string, timeStr string) (time.Time, error) {
	locationName := name
	if l, err := time.LoadLocation(locationName); err != nil {
		println(err.Error())
		return time.Time{}, err
	} else {
		lt, _ := time.ParseInLocation(timeLayout, timeStr, l)
		fmt.Println(locationName, lt)
		return lt, nil
	}
}

