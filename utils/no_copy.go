package utils

import "fmt"

type noCopy struct{}

func (*noCopy) Lock()   {}
func (*noCopy) Unlock() {}


type T struct {
	noCopy
}

//注意，即使包含了NoCopy的结构体，也不是真正的就不可复制了，实际上毫无影响，无论是编译，还是运行都毫不影响
//只有在go vet命令下，才会提示出其中的nocopy问题，区别与c++的拷贝构造函数
func test(t T) {
	fmt.Println(t)
}

