package utils

import (
	"bufio"
	"fmt"
	"io"
	"math/rand"
	"net"
	"strconv"
	"strings"
	"sync"
	"time"
)

type redisPrefix string

const (
	SingleRowPrefix redisPrefix = "+" //`单行回复`
	ErrorPrefix     redisPrefix = "-" //`错误回复`

	IntPrefix      redisPrefix = ":" //`整型数字`
	MultiRowPrefix redisPrefix = "$" //`多行字符串`
	ArrayPrefix    redisPrefix = "*" //`数组`
)

type redisCommand string

const (
	Set redisCommand = "SET"
)

const NewLine string = "\r\n"
const Nil string = "nil"
const Null string = ""
const OK string  = "OK"

type Client struct {
	Addr string
	Auth string
	Db uint8
	conn net.Conn
}

type cli interface {
	Get(key string) (string, error)
	Set(key string, val string) error
	Del(key ...string) error
	Expire(key string, t uint32) error
	FlushAll() error
	Ttl(key string) (int64, error)
	Db(index uint) error
	Keys(pattern string) ([]string, error)
	MGet(keys ...string) ([]string, error)
	Close() error
}

type Call interface {
	call(commands[] string) (interface{}, error)
}

type Cli struct {
	mutex sync.Mutex
	c *Client
}

func Init(addr string, auth string, db uint8) *Cli {
	c := &Cli{}
	client := &Client{Auth:auth, Addr:addr}
	c.c = client
	tcpAddr, err := net.ResolveTCPAddr("tcp4", addr)
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	client.Db = db
	if err != nil {
		panic(err)
	}
	client.conn = conn
	if len(auth) > 0 {
		err = c.Auth(auth)
		if err != nil {
			panic(err)
		}
	}
	err = c.Db(db)
	if err != nil {
		panic(err)
	}
	return c
}
//func
func (c *Cli) Get(key string) (string, error) {
	if len(key) <= 0 {
		return Null, fmt.Errorf("param error")
	}
	param, _ := makeRequest("GET", key)
	buf, err := c.call(param)
	if err != nil {
		return Null, err
	}
	s, err := parseResponse(buf)
	if err == nil {
		return s.(string), nil
	}
	return Null, err
}


func (c *Cli) Set(key string, val string) error {
	if len(key) <= 0 || len(val) <= 0 {
		return fmt.Errorf("none key or val found")
	}
	param, _ := makeRequest("SET", key, val)
	ret, err := c.call(param)
	_, err = parseResponse(ret)
	return err
}

func (c *Cli) Del(key ...string) error {
	if len(key) <= 0 {
		return nil
	}
	keys := append([]string{"DEL"}, key...)
	param, _ := makeRequest(keys...)
	_, err := c.call(param)
	return err
}

func (c *Cli) FlushAll() error {
	param, err := makeRequest("FLUSHALL")
	if err != nil {
		return fmt.Errorf("param error")
	}
	ret, err := c.call(param)
	if err == nil {
		fmt.Println(string(ret))
	}
	return err
}

func (c *Cli) Expire(key string, t uint32) error {
	if len(key) <= 0 {
		return nil
	}
	if t <= 0 {
		return c.Del(key)
	}
	param, _ := makeRequest("EXPIRE", key, strconv.FormatInt(int64(t), 10))
	_, err := c.call(param)
	return err
}

func (c *Cli) Ttl(key string) (int64, error) {
	if len(key) <= 0 {
		return -2, fmt.Errorf("param error")
	}
	param, _ := makeRequest("TTL", key)
	t, err := c.call(param)
	if err == nil {
		r := string(t)
		if len(r) > 0 && strings.Index(r, ":") == 0 {
			ttl, err := strconv.Atoi(strings.Replace(strings.TrimSpace(r), ":", "", 1))
			if err == nil {
				return int64(ttl), nil
			}
		}
	}
	return -2, nil
}


func (c *Cli) Auth(auth string) error {
	if len(auth) <= 0 {
		return nil
	}
	param, _ := makeRequest("AUTH", auth)
	buf, err := c.call(param)
	_, err = parseResponse(buf)
	return err
}

func (c *Cli) Db(idx uint8) error {
	param, _ := makeRequest("SELECT",  fmt.Sprintf("%d", idx))
	_, err := c.call(param)
	return err
}

func (c *Cli) Keys(pattern string) (ret []string, err error) {
	ret, err = make([]string, 0), nil
	if len(pattern) <= 0 {
		err = fmt.Errorf("param error, no pattern found")
	}
	param, _ := makeRequest("KEYS", pattern)
	buf, err := c.call(param)
	if err == nil {
		r, err := parseResponse(buf)
		if err == nil {
			ret = r.([]string)
		}
	}
	return
}

func makeRequest(cs ...string) ([] string, error) {
	if len(cs) <= 0 {
		return nil, fmt.Errorf("no param found")
	}
	ret := make([]string, 8)
	ret = append(ret, fmt.Sprintf("%s%d", ArrayPrefix, len(cs)), NewLine)
	for _, c := range cs {
		if len(strings.TrimSpace(c)) <= 0 {
			return nil, fmt.Errorf("unexpect param found")
		} else {
			ret = append(ret, fmt.Sprintf("%s%d", MultiRowPrefix, len(c)), NewLine, c, NewLine)
		}
	}
	return ret, nil
}

func parseResponse(buf []byte) (ret interface{}, err error) {
	if buf == nil || len(buf) <= 0 {
		return
	}

	str := string(buf)
	tmp := strings.Split(str, NewLine)
	switch {
	case strings.HasPrefix(tmp[0], string(SingleRowPrefix)):
		ret = strings.Replace(tmp[0], string(SingleRowPrefix), Null, 1)
	case strings.HasPrefix(tmp[0], string(ErrorPrefix)):
		err = fmt.Errorf(strings.Replace(tmp[0], string(ErrorPrefix), Null, 1))
	case strings.HasPrefix(tmp[0], string(IntPrefix)):
		ret, err = strconv.Atoi(strings.Replace(tmp[0], string(IntPrefix), Null, 1))
	case strings.HasPrefix(tmp[0], string(MultiRowPrefix)):
		ret = tmp[1]
	case strings.HasPrefix(tmp[0], string(ArrayPrefix)):
		count,_ := strconv.Atoi(strings.Replace(tmp[0], string(ArrayPrefix), Null, 1))
		s := make([]string, 0)
		if count > 0 {
			for i := 1; i<=count; {
				s = append(s, tmp[i * 2])
				i++
			}
		}
		ret = s
	default:
	}
	return
}

func (c *Cli) call(commands []string) ([]byte, error) {
	if len(commands) <= 0 {
		return nil, fmt.Errorf("no call found")
	}
	s := Null
	for _,e := range commands[0:] {
		if len(e) > 0 {
			s += e
		}
	}
	c.mutex.Lock()
	defer c.mutex.Unlock()
	var out io.Writer
	out = c.c.conn
	_, err := out.Write([]byte(s))
	if err != nil {
		return nil, err
	}

	var in io.Reader = c.c.conn
	str, err := ensureFull(c.c.conn)
	if err != nil {
		return nil, err
	}
	if isPackageIntegral(str) {
		return []byte(str), nil
	} else if strings.HasPrefix(str, string(MultiRowPrefix)) {
		tmpStr, err := ensureFull(in)
		if err != nil {
			return nil, err
		}
		str += tmpStr
		return []byte(str), nil
	} else {
		count, _ := strconv.Atoi(str[1:len(str) - 2])
		for i := 0; i < count; {
			tmpStr, err := ensureFull(in)
			if err != nil {
				return nil, err
			}
			str += tmpStr
			if isPackageIntegral(tmpStr) {
				i++;
			}
		}
		return []byte(str), nil
	}
}

func (c *Cli) Close() error {
	return c.c.conn.Close()
}

func (c *Cli) MGet(key ...string) ([]string, error) {
	if key == nil || len(key) <= 0 {
		return nil, nil
	}
	keys := []string {"MGET"}
	param, _ := makeRequest(append(keys, key...)...)
	buf, err := c.call(param)
	if err != nil {
		return nil, err
	}
	parseResponse(buf)
	return nil, err
}

func ensureFull(in io.Reader) (string, error) {
	str, err := bufio.NewReader(in).ReadString('\n')
	if err != nil {
		return Null, err
	}
	if str[len(str) - 2] == '\r' {
		return str, nil
	} else {
		buffer := make([]byte, 0, 1024)
		buffer = append(buffer, []byte(str)...)
		for {
			s, err := bufio.NewReader(in).ReadString('\n')
			if err != nil {
				return Null, err
			}
			if s[len(s) - 2] == '\r' {
				str += s
			}
		}
		return str, nil
	}
}

func randStr() string {
	return fmt.Sprintf("%d", rand.Uint32())
}

func produce(c *Cli, ch *chan string) {
	key, val := randStr(), randStr()
	_ = c.Set(key, val)
	fmt.Printf("produce %s:%s\n", key, val)
	*ch <- key
	time.Sleep(1000 * time.Millisecond)
}

func consume(c *Cli, ch *chan string) {
	var key string
	for {
		select {
		case key = <-*ch:
			val, err := c.Get(key)
			if err == nil {
				fmt.Printf("consume %s:%s\n", key, val)
			}
			time.Sleep(1000 * time.Millisecond)
		default:
		}
	}
}

func isPackageIntegral(s string) bool {
	return strings.HasPrefix(s, string(SingleRowPrefix)) ||
		strings.HasPrefix(s, string(ErrorPrefix)) ||
		strings.HasPrefix(s, string(IntPrefix))
}

func main() {
	rand.Seed(time.Now().Unix())

	host := "vm:6379"
	auth := ""
	cli := Init(host, auth, 0)

	defer cli.Close()
	//cli.Set("hello", "golang")
	//r, err := cli.Get("hello")
	//if err == nil {
	//	fmt.Println(r)
	//}
	//cli.Del("hello")
	//r, err = cli.Get("hello")
	//if err == nil {
	//	fmt.Println(r)
	//}
	//ch := make(chan string)
	//go consume(cli, &ch)
	//for {
	//	produce(cli, &ch)
	//}
	err := cli.Auth("")
	if err != nil {
		fmt.Println(err)
	} else {
		//cli.Db(0)
		//
		str := Null
		for i:=0; i<66666; i++ {
			str += "x"
		}
		_ = cli.Set("hello", str)
		_ = cli.Set("hell", "world")
		//r, err := cli.Get("hello")
		//if err == nil {
		//	fmt.Println(r)
		//}
		//fmt.Println(cli.Get("hello"))
		fmt.Println(cli.MGet("hello", "hell"))
		//fmt.Println(cli.Get("hello"))
		//keys, _ := cli.Keys("*")
		//if keys != nil && len(keys) > 0 {
		//	for _, k := range keys {
		//		fmt.Println(cli.Get(k))
		//	}
		//}
		//fmt.Println(cli.Keys("*"))
	}

	//cli.Expire("hello", 180)
	////fmt.Println(cli.Get("hello"))
	//keys, err := cli.Keys("*")
	//if err == nil {
	//	fmt.Println(keys)
	//	cli.Del(keys[0:]...)
	//}
	//_ = cli.Db(0)
	//_ = cli.Set("hello", "" +
	//	"A client implementation may return different kind of exceptions for different errors, or may provide a generic way to trap errors by directly providing the error name to the caller as a string.However, such a feature should not be considered vital as it is rarely useful, and a limited client implementation may simply return a generic error condition, such as false.\n" +
	//	"A client implementation may return different kind of exceptions for different errors, or may provide a generic way to trap errors by directly providing the error name to the caller as a string.However, such a feature should not be considered vital as it is rarely useful, and a limited client implementation may simply return a generic error condition, such as false.")
	//_ = cli.Expire("hello", 180)
	//fmt.Println(cli.Get("hello"))
	//	cli.Set("hello", "hhh")
	//	cli.Expire("hello", 888)
	//for {
	//	ttl, err := cli.Ttl("hello")
	//	if err ==  nil {
	//		fmt.Println(ttl)
	//		if ttl < 0 {
	//			break
	//		}
	//time.Sleep(1000 * time.Millisecond)
	//	} else {
	//		break
	//	}
	//}
	//cli.FlushAll()
}
