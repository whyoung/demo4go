package netx

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func HttpGet(url string, header map[string]string) string {
	client := &http.Client{}
	request, err := http.NewRequest("GET", url, nil)

	if err != nil {
		panic(err)
	}

	if header != nil {
		for key, value := range header {
			request.Header.Add(key, value)
		}
	}

	response, err := client.Do(request)

	if err != nil {
		panic(err)
	} else {
		defer response.Body.Close()
	}

	if response.StatusCode != 200 {
		panic(fmt.Errorf("error accured, status code %d", response.StatusCode))
	}

	bs, err := ioutil.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}

	return string(bs)
}