package netx

import (
	"fmt"
	"golang.org/x/net/icmp"
	"golang.org/x/net/ipv4"
	"net"
	"syscall"
)

func RawSocketServer() {
	netaddr, _ := net.ResolveIPAddr("ip4", "192.168.40.128")
	conn, _ := net.ListenIP("ip4:tcp", netaddr)
	ipconn, _ := ipv4.NewRawConn(conn)
	for {
		buf := make([]byte, 1500)
		hdr, payload, controlMessage, _ := ipconn.ReadFrom(buf)
		if "192.168.40.1" == fmt.Sprintf("%v", hdr.Src) {
			continue
		}
		fmt.Println("ipheader:")
		fmt.Println("hdr:", hdr)
		fmt.Println("payload:",payload)
		fmt.Println("controlMessage:", controlMessage)
	}
}


func RawSocketClient() {fd, _ := syscall.Socket(syscall.AF_INET, syscall.SOCK_RAW, syscall.IPPROTO_RAW)
	addr := syscall.SockaddrInet4{
		Port: 0,
		Addr: [4]byte{192, 168, 40, 128},
	}
	yipHeader := ipv4.Header{
		Version:  4,
		Len:      20,
		TotalLen: 20, // 20 bytes for IP, 10 for ICMP
		TTL:      64,
		Protocol: 6, // TCP
		//自定义ip协议的源地址和目的地址
		Dst:      net.IPv4(192, 168, 40, 128),
		Src:      net.IPv4(10, 93, 245, 222),
	}
	payload, _ := yipHeader.Marshal()
	payload = append(payload, make([]byte, 20)...)
	fmt.Println(syscall.Sendto(fd, payload, 0, &addr))
}

func RawSocketClient2() {

	fd, _ := syscall.Socket(syscall.AF_INET, syscall.SOCK_RAW, syscall.IPPROTO_RAW)

	addr := syscall.SockaddrInet4{
		Port: 0,
		Addr: [4]byte{192, 168, 40, 128},
	}
	yipHeader := ipv4.Header{
		Version:  4,
		Len:      20,
		TotalLen: 20, // 20 bytes for IP, 10 for ICMP
		TTL:      64,
		Protocol: 6, // TCP
		//自定义ip协议的源地址和目的地址
		Dst:      net.IPv4(192, 168, 40, 128),
		Src:      net.IPv4(10, 93, 245, 222),
	}
	payload, _ := yipHeader.Marshal()
	payload = append(payload, make([]byte, 20)...)
	fmt.Println(syscall.Sendto(fd, payload, 0, &addr))
}

func main() {
	netaddr, _ := net.ResolveIPAddr("ip4", "192.168.40.128")
	conn, _ := net.ListenIP("ip4:icmp", netaddr)
	for {
		buf := make([]byte, 1024)
		n, addr, _ := conn.ReadFrom(buf)
		msg,_:=icmp.ParseMessage(1,buf[0:n])
		fmt.Println(n, addr, msg.Type,msg.Code,msg.Checksum)
	}
}

func RawTcp() {
	fd, _ := syscall.Socket(syscall.AF_INET, syscall.SOCK_RAW, syscall.IPPROTO_RAW)

	//
	addr := syscall.SockaddrInet4{
		Port: 6666,
		Addr: [4]byte{127, 0, 0, 1},
	}

	tcpHeader := TCPHeader{
		Source:      6666,
		Destination: 9999,
		SeqNum:      3643,
		AckNum:      0,
		DataOffset:  5,
		Reserved:    0,
		ECN:         0,
		Ctrl:        2,
		Window:      255,
		Checksum:    0,
		Urgent:      0,
		Options:     nil,
	}

	ack := tcpHeader.Marshal()

	ipHeader := ipv4.Header{
		Version:  4,
		Len:      20,
		TotalLen: 20, // 20 bytes for IP
		TTL:      64,
		Protocol: 6, // TCP
		Dst:      net.IPv4(127, 0, 0, 1),
		Src:      net.IPv4(127, 0, 0, 1),
	}


	payload, _ := ipHeader.Marshal()
	syscall.Sendto(fd, payload, 0, &addr)


	//
	//buf := make([]byte, 1024)
	//syscall.Recvfrom(fd, buf)

}