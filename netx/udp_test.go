package netx

import (
	"fmt"
	"net"
	"sync"
	"testing"
)

func TestUdpRecv(t *testing.T) {
	w := sync.WaitGroup{}
	w.Add(1)
	addr, err := net.ResolveUDPAddr("udp", "127.0.0.1:22123")
	conn, err:= net.ListenUDP("udp", addr)
	if err != nil {
		panic(err)
	}
	buf := make([]byte, 0, 1024)

	//no block
	n, raddr, err := conn.ReadFromUDP(buf)
	fmt.Println(err)
	for n == 0 {
		n, raddr, err = conn.ReadFromUDP(buf)
	}
	fmt.Println(raddr)
	fmt.Println(n)
	fmt.Println(string(buf[:n]))
	w.Wait()
}

func TestUdpSend(t *testing.T) {
	addr, err := net.ResolveUDPAddr("udp", "127.0.0.1:22122")
	conn, err:= net.DialUDP("udp", nil, addr)
	if err != nil {
		panic(err)
	}
	n, _ := conn.Write([]byte("hello"))
	fmt.Printf("send %d \n", n)
}