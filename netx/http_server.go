package netx

import (
	"io/ioutil"
	"log"
	"net/http"
)

func echo(wr http.ResponseWriter, r *http.Request) {
	msg, err := ioutil.ReadAll(r.Body)
	if err != nil {
		_, _ = wr.Write([]byte("echo error"))
		return
	}

	writeLen, err := wr.Write(msg)
	if err != nil || writeLen != len(msg) {
		log.Println(err, "write len:", writeLen)
	}
}

func htmlTest(wr http.ResponseWriter, r *http.Request) {
	var msg = `
	<html>
		<body><h1>hello wolld</h1></body>
	</html>
	`
	_, _ = wr.Write([]byte(msg))
}

func jsonTest(wr http.ResponseWriter, r *http.Request) {

	wr.Header().Set("Content-type", "application/json")
	msg := `{"key":"hello", "value":"world"}`
	_, _ = wr.Write([]byte(msg))
}

func redirectTest(wr http.ResponseWriter, r *http.Request) {
	wr.WriteHeader(http.StatusFound)
}