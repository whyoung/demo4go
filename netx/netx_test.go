package netx

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
	"os"
	"testing"
)

func TestWebsocketServer(t *testing.T) {
	http.HandleFunc("/echo", Echo)
	fmt.Println("start server  port:9091")
	if err := http.ListenAndServe(":9091", nil); err != nil {
		log.Fatal("ListenAndServe:1234:", err)
	}
}

func TestHttpServer(t *testing.T) {
	http.HandleFunc("/", echo)
	http.HandleFunc("/html", htmlTest)
	http.HandleFunc("/json", jsonTest)
	http.HandleFunc("/rest/:v", jsonTest) //rest api
	http.HandleFunc("/redirect", redirectTest) //rest api
	mux := httprouter.New()
	err := http.ListenAndServe(":8080", mux)
	if err != nil {
		os.Exit(1)
	}
}

func TestHttpGet(t *testing.T) {
	ret := HttpGet("http://localhost:8080/json", nil)
	fmt.Print(ret)
}

func TestRawSocketServer(t *testing.T) {
	RawSocketServer()
}

func TestTestRawSocketClient(t *testing.T) {
	RawSocketClient()
}

func TestStartDomainSockServer(t *testing.T) {
	StartDomainSockServer()
}

func TestStartDomainSockClient(t *testing.T) {
	StartDomainSockClient()
}

func TestStartServer(t *testing.T) {
	StartServer()
}