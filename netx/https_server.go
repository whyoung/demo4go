package netx

import (
	"io"
	"log"
	"net/http"
)

func HttpsServer() {
	http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		req.Header.Add("Content-Type", "application/json")
		io.WriteString(w, "{\"hello\": \"world!\"")
	})
	//第二个参数是服务端的证书，第三个参数是服务端的密钥
	if e := http.ListenAndServeTLS(":8080", "server.crt", "server.key", nil); e != nil {
		log.Fatal("ListenAndServe: ", e)
	}
}
