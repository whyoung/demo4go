package main

import (
	"bitbucket.org/whyoung/demo/rpc/registry"
	"bitbucket.org/whyoung/demo/rpc/service"
)

func main() {
	s := service.BaseDemoService()
	//s.RegisterOption(server.NewGRPCServer())
	s.RegisterOption(registry.NewZookeeperRegistryOption())
	s.StartService()
}
