package sdk

import (
	"fmt"
	"testing"
)

//指针逃逸
func TestReturnLocalPointer(t *testing.T) {
	p := localPointer()
	*p += 3
	fmt.Println(*p == 7)
}

func localPointer() *int {
	i := 4
	return &i
}
