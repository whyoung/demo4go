package sdk

import (
	"fmt"
	"reflect"
	"testing"
)

type innerIface interface {
	Method()
}

type innerStruct struct {
	M map[string] interface{}
	C chan bool
	N *int
	S string

}

func (s innerStruct) String() string {
	return "inner struct"
}

func (s innerStruct) Method() {
	fmt.Println(s)
}

func (s innerStruct) F() {
	s.Method()
}


func TestTypeOf(t *testing.T) {
	n := 6379
	s := innerStruct{
		M: make(map[string]interface{}),
		C: make(chan bool),
		N: &n,
		S: "go go go",
	}

	var i interface{} = s
	to := reflect.TypeOf(i)
	fmt.Println(to)

	vo := reflect.ValueOf(i)
	//如果已知类型，使用类型断言转换成已知类型即可

	/*
		1.转换的时候，如果转换的类型不完全符合，则直接panic，类型要求非常严格！
		2.转换的时候，要区分是指针还是值
		3.反射可以将“反射类型对象”再重新转换为“接口类型变量”
	*/

	is, ok := vo.Interface().(innerStruct)
	if ok {
		fmt.Println("bingo")
		fmt.Println(is)
	}

	//怎么理解 == 运算符
	//fmt.Println(vo.Interface() == i)

	//如果是未知类型

	// 获取方法字段
	// 1. 先获取interface的reflect.Type，然后通过NumField进行遍历
	// 2. 再通过reflect.Type的Field获取其Field
	// 3. 最后通过Field的Interface()得到对应的value
	for i := 0; i < to.NumField(); i++ {
		field := to.Field(i)
		if Exported(field.Name) {
			//必须要保证属性是可导出的，否则会引发panic
			value := vo.Field(i)
			fmt.Printf("%s: %v = %v\n", field.Name, field.Type, value.Interface())
		}
	}

	// 获取方法
	// 1.先获取interface的reflect.Type，然后通过.NumMethod进行遍历，注意只返回导出的方法数
	for i := 0; i < to.NumMethod(); i++ {
		m := to.Method(i)
		fmt.Printf("%s: %v\n", m.Name, m.Type)
	}

	field := vo.FieldByName("N")
	fmt.Println(field)

	// 通过reflect.ValueOf获取num中的reflect.Value，注意，参数必须是指针才能修改其值
	// 获取原始值对应的反射对象
	pointer := field.Elem()

	pointer.SetInt(3306)

	fmt.Println(*s.N)

	m := vo.MethodByName("F")
	m.Call(nil)
}

func Exported(name string) bool {
	if name == "" {
		return false
	}
	c := name[0]
	return c >= 'A' && c <= 'Z'
}



type s struct {}

type ss struct {
	s
}

func main() {
	var x interface{}
	x = ss{}

	_, ok := x.(s)
	fmt.Println(ok)
}