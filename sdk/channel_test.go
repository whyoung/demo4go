package sdk

import (
	"fmt"
	"strings"
	"testing"
	"time"
)

//channel 本身就是引用类型，不需要传递指针
func run(i int, stop chan bool) {
	for {
		//以非阻塞的方式读取channel中的数据
		select {
		case <-stop:
			fmt.Printf("监控 %d 退出，停止了...\n", i)
			return
		default:
			fmt.Printf("goroutine %d 监控中...\n", i)
			<- time.After(time.Second)
		}
	}
}

func TestChannel(t *testing.T) {
	stop := make(chan bool)

	for i := 0; i<5; i++ {
		i := i
		go run(i, stop)
	}

	time.Sleep(10 * time.Second)
	fmt.Println("可以了，通知监控停止")
	for i := 0; i<5;i++ {
		stop <- true
	}

	<- time.After(5 * time.Second)
}


func TestChannelRange(t *testing.T) {
	ch := make(chan string)
	go func() {
		for s := range ch {
			println(strings.ToUpper(s))
		}
		//当close 执行后会跳出range 循环
		println("go routine exit")
	}()

	for i := 0;i<10;i++ {
		ch <- fmt.Sprintf("ab%dEdqw", i)
	}
	time.Sleep(5 * time.Second)
	close(ch)
	time.Sleep(5 * time.Second)
}

func TestChannelClose(t *testing.T) {

	ch := make(chan int)

	f := func(id int) {
		fmt.Printf("routine %d receive %d\n", id, <-ch)
		fmt.Printf("routine %d quit\n", id)
	}

	go f(1)
	go f(2)
	go f(3)

	time.Sleep(time.Second * 5)
	ch <- 999

	//关闭channel时，所有阻塞在chan读上的routine 都会收到对应类型的零值
	close(ch)
	time.Sleep(time.Second * 5)

	/*
	routine 2 receive 0
	routine 2 quit
	routine 3 receive 0
	routine 3 quit
	routine 1 receive 999
	routine 1 quit
	 */
}

func TestChannelCloseWithZero(t *testing.T) {
	ch := make(chan interface{})

	f := func(id int) {
		fmt.Printf("routine %d receive %d\n", id, <-ch)
		fmt.Printf("routine %d quit\n", id)
	}

	go f(1)
	go f(2)
	go f(3)

	time.Sleep(time.Second * 5)
	ch <- "ctrl + c"
	close(ch)

	/*
	routine 3 receive %!d(<nil>)
	routine 3 quit
	routine 2 receive %!d(<nil>)
	routine 2 quit
	routine 1 receive %!d(string=ctrl + c)
	routine 1 quit
	 */
	time.Sleep(time.Second * 5)
}

//routine 泄漏
func TestBlockLeak(t *testing.T) {
	ch := make(chan bool)
	go func() {
		println("processing...")
		<- ch
		println("done")
	}()
}

func TestUnlimitedLoopLeak(t *testing.T) {
	ch := make(chan bool, 1)
	go func() {
		for {
			select {
			case <-ch:
				println("done")
			default:
				time.Sleep(time.Second)
			}
		}
	}()
}
