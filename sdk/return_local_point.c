#include <stdio.h>
#include <stdlib.h>

//c 语言的局部变量一定是存放在栈内存空间，malloc的除外
int *localPointer() {
    int i = 4;
    //compile error return-stack-address
    return &i;
}

int *staticLocalPointer() {
    static int i = 4;
    return &i;
}

int main(int argc, char *argv[]) {
	int *p = localPointer();
	printf("%d\n", *p);

	int *sp = staticLocalPointer();
	*sp = *sp + 3;
	printf("%d\n", *sp);
	return 0;
}