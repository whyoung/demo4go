package sdk

import (
	"fmt"
	"reflect"
	"testing"
)

//自定义错误
type divideZeroError struct {
}

func (err divideZeroError) Error() string {
	return "divide by zero"
}

func divide(a, b int) (int, *divideZeroError) {
	if b == 0 {
		return 0, &divideZeroError{}
	} else {
		return a/ b, nil
	}
}

func TestErrorNil(t *testing.T) {
	var err error
	fmt.Println(reflect.TypeOf(err) == nil)
	if err != nil {
		fmt.Println(reflect.ValueOf(err).IsNil())
	}

	ret, err := divide(2, 1)
	if err == nil {
		println(ret)
	} else {
		fmt.Println(reflect.TypeOf(err).Elem().Kind())
		fmt.Println(reflect.ValueOf(err).IsNil())

		println(err)
		panic(err)
	}
}
