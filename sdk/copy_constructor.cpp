#include <iostream>

using namespace std;

class demo {		
	public:
		int *p;
		char c;
		demo(char cc) {
			p = (int *)malloc(sizeof(int));
			*p = 1;
			c = cc;
		}
		
		~demo() {
			free(p);
		}

		//拷贝构造函数，当函数参数是值类型时，会调用这个方法构造新的参数值，涉及到深拷贝和浅拷贝的问题
		//如果把拷贝构造函数设置成private，表示禁止拷贝
		// 私有化运算符重载，禁止使用赋值运算
		// demo& operator=(const demo&);
		demo(const demo& d) {
			//深拷贝
			p = (int *)malloc(sizeof(int));
			*p = *d.p;

			//p = d.p; 浅拷贝
			c = d.c;

			/*
		    默认拷贝构造函数的实现
			p = d.p;
			c = d.c;
			*/
		}
};

//将会调用拷贝构造函数
void test(demo d) {
	d.c = 'b';
	*d.p += 1;
}

int main(int argc, char *argv[]) {
	demo d = demo('a');
	test(d);
	cout<<d.c<<endl;    //a
	cout<<*d.p<<endl;   //3，如果是默认拷贝构造函数结果是4
	return 0;
}