package sdk

import (
	"fmt"
	"testing"
)

//defer 的函数是nil，引发panic
func TestDeferNil(t *testing.T) {
	var f func()
	defer f()
}

/**
对于以下两种情况
func() int {
	i := 1
	defer func() {
		i += 3
	}()
	return i
}

实际的执行如下
func() int {
	r := 0
	i := 1
	defer func() {
		i += 3
	}
	r = i //此时i == 1，所以defer中对局部变量的修改不会反映到返回值中
	return r
}

而对于下面的程序：
func() (r int) {
	r = 1
	defer func() {
		r += 3 //会修改r变量所对应的内存的值
	}
	r = i //此时r == 1
	return r //4
}

 */
func TestDeferAfterReturn(t *testing.T) {
	f := func() int {
		i := 1
		defer func() {
			i = i+3
		}()
		return i
	}
	fmt.Println(f()) //1
}

func TestDeferAfterReturn2(t *testing.T) {
	f := func() (r int) {
		r = 1
		defer func() {
			r = r+3
		}()
		return r
	}
	fmt.Println(f()) //4
}

func TestClosureOuterRef(t *testing.T) {
	f := func() int {
		var i = 1
		p := &i
		defer func() {
			*p = *p+3
		}()
		fmt.Println(i)
		return i
	}
	fmt.Println(f())
}

func TestClosureParam(t *testing.T) {
	f := func() int {
		var i = 1
		p := &i
		defer func(p *int) {
			*p = *p+3
		}(p)
		fmt.Println(i)
		return i
	}

	fmt.Println(f()) //1
}