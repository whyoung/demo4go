package sdk

import (
	"context"
	"testing"
	"time"
)

func process(ctx context.Context) {
	go process11(ctx)
	go process12(ctx)
}

func process11(ctx context.Context) {
	go process21(ctx)
	for {
		select {
		case <-ctx.Done():
			return
		default:
			println("process 1...")
			time.Sleep(time.Second)
		}
	}
}

func process12(ctx context.Context) {
	process22(ctx)
}

func process21(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		default:
			println("process 2...")
			time.Sleep(time.Second)
		}
	}
}

func process22(ctx context.Context) {
	process31(ctx)
}

func process31(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		default:
			println("process 3 ...")
			time.Sleep(time.Second)
		}
	}
}

func TestContextCancel(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	process(ctx)
	time.Sleep(10 * time.Second)
	cancel()
}
