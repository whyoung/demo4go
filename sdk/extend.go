package sdk

type iFace interface {
	m()
}

type impl struct {
}

//虽然是以组合方式，但具有继承的属性，extends 也是实现了 iFace 接口
type extend struct {
	impl
}

func (i impl) m() {

}
