package sdk

import (
	"fmt"
	"sort"
	"testing"
)

func TestSortString(t *testing.T) {

	strs := []string{"9", "3", "2", "1", "7", "8", "6", "4", "5"}
	sort.Strings(strs)
	fmt.Println(strs)
}

//to sorted struct
type tos int

//需要实现 sort.Interface 接口
type toss []tos

func (t toss) Len() int {
	return len(t)
}

func (t toss) Less(i, j int) bool {
	return t[i] < t[j]
}

func (t toss) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}

func TestSortStruct(t *testing.T) {
	var toss []tos
	toss = append(toss, 4, 5,6,8,9,0,2,1,7)
	fmt.Println(toss)
}