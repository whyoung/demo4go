package sdk


import (
	"fmt"
	"reflect"
)

type Int int

type S struct {
	I Int
	s string
}

func main() {
	s := S{Int(2), "str"}
	fmt.Println(s)

	t := reflect.TypeOf(s)  //Type
	fmt.Println(t)
	//kind返回的是接口类型变量的底层的实际类型
	fmt.Println(t.Kind())	//Kind（uint)，通用的类型，int，struct，prt等
	fmt.Println(reflect.ValueOf(s)) //Value

	ps := &s
	t = reflect.TypeOf(ps)  //Type
	fmt.Println(t)
	fmt.Println(t.Kind())	//指针类型

	//求指针类型所指向地址的内容
	v := reflect.ValueOf(ps)  //Value
	fmt.Println(v.Interface() == ps)
	fmt.Println(v.Elem().Interface() == s)

	//Value.Addr 获取Value的值所对应的指针
	fmt.Println(v.Elem().Addr().Interface() == ps)

	pps := &ps
	fmt.Println(reflect.TypeOf(pps).Elem().Elem().Kind())
}