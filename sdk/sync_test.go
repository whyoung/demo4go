package sdk

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

func TestWaitGroup(t *testing.T) {
	w := sync.WaitGroup{}

	f := func(id int) {
		fmt.Printf("routine %d start\n", id)
		time.Sleep(time.Second)
		fmt.Printf("routine %d finish\n", id)
		w.Done()
	}

	for i:=1; i<5; i ++ {
		w.Add(1)
		i := i
		go f(i)
	}
	<- time.After(5 * time.Second)

	fmt.Println("main routine finish")
}

func TestLock(t *testing.T) {
	mutex := sync.Mutex{}
	w := sync.WaitGroup{}
	w.Add(2)

	f := func(id int) {
		mutex.Lock()
		fmt.Printf("routine %d locked\n", id)
		time.Sleep(time.Second * 5)
		mutex.Unlock()
		fmt.Printf("routine %d unlocked\n", id)
		w.Done()
	}
	go f(1)
	go f(2)
	w.Wait()
	fmt.Println("main routine quit")
}


func TestRWLock(t *testing.T) {
	mutex := sync.RWMutex{}
	w := sync.WaitGroup{}
	w.Add(4)

	read := func(id int) {
		mutex.RLock()
		fmt.Printf("read routine %d locked\n", id)
		time.Sleep(time.Second * 10)
		mutex.RUnlock()
		fmt.Printf("read routine %d unlocked\n", id)
		w.Done()
	}
	go read(1)
	go func() {
		mutex.Lock()
		fmt.Println("write routine locked")
		time.Sleep(time.Second * 10)
		mutex.Unlock()
		fmt.Println("write routine unlocked")
		w.Done()
	}()
	go read(2)

	time.Sleep(time.Second * 5)
	go read(3)
	w.Wait()
	fmt.Println("main routine quit")
}
