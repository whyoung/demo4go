package sdk

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"testing"
)

// 监听指定信号
func TestSignal(t *testing.T)  {
	//创建chan
	c := make(chan os.Signal)

	//监听指定信号 ctrl+c kill
	signal.Notify(c, os.Interrupt, os.Kill, syscall.SIGUSR1, syscall.SIGUSR2)
	//阻塞直到有信号传入
	fmt.Println("启动")
	//阻塞直至有信号传入
	s := <-c
	fmt.Println("退出信号", s)
}